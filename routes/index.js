var express = require('express');
var router = express.Router();
const request = require('request');

const apiKey = '';

const apiBaseUrl = 'http://api.themoviedb.org/3';
const imageBaseUrl = 'https://image.tmdb.org/t/p/w300';

router.use((req, res, next) => {
  res.locals.imageBaseUrl = imageBaseUrl;
  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
  const nowPlayingUrl = `${apiBaseUrl}/movie/now_playing?api_key=${apiKey}`;
  request.get(nowPlayingUrl, (error, response, movieData) => {
    const parsedMovieData = JSON.parse(movieData);
    res.render('index', { movies: parsedMovieData.results });
  });
});

router.get('/movie/:id', (req, res, next) => {
  const movieId = req.params.id;
  const movieUrl = `${apiBaseUrl}/movie/${movieId}?api_key=${apiKey}`;
  request.get(movieUrl, (error, response, movieData) => {
    res.render('single-movie', { movie:  JSON.parse(movieData) } );
  });
});

router.post('/search', (req, res, next) => {
  const cat = req.body.cat;
  const searchTerm = encodeURI(req.body.movieSearch);
  const searchUrl =  `${apiBaseUrl}/search/${cat}?query=${searchTerm}&api_key=${apiKey}`;
  request.get(searchUrl, (error, response, movieData) => {
    const parsedData = JSON.parse(movieData).results;
    const movies = cat == 'movie' ? parsedData : parsedData.map(person => person.known_for).flat();
    res.render('index', { movies });
  });
});

module.exports = router;
